<?php
 
!defined( 'ABSPATH' ) AND exit;

/*

Plugin Name: 	Custom Installation Script
Plugin URI:		http://wordpress.stackexchange.com/q/75420/12615
Description:	Create our own content on WP install
License: GPL

*/
 
 
 

/*
*
*
* Plugin auto-activation function
*
* http://wordpress.stackexchange.com/q/4041/12615
*
*
*
*/


function wpse_4041_run_activate_plugin( $plugin ) {
	$current = get_option( 'active_plugins' );
	$plugin = plugin_basename( trim( $plugin ) );
	 
	if( !in_array( $plugin, $current ) ) {
			$current[] = $plugin;
			sort( $current );
			do_action( 'activate_plugin', trim( $plugin ) );
			update_option( 'active_plugins', $current );
			do_action( 'activate_' . trim( $plugin ) );
			do_action( 'activated_plugin', trim( $plugin ) );
		}
	 
	return null;
}
 
 
/*
*
* Overriding WordPress custom install defaults
* Removed ALL Multisite functionality from the original wp_install_defaults()
*
* http://wordpress.stackexchange.com/a/75436/12615
*
*
*/




function wp_install_defaults( $user_id ) {
	global $wpdb, $wp_rewrite, $current_site, $table_prefix;
	
	// Default category
	$cat_name = __( 'Frum Category' );
	$cat_slug = sanitize_title( _x( 'Frum Category', 'Default category slug' ) );
	 
	if( global_terms_enabled() ) {
		$cat_id = $wpdb->get_var(
			$wpdb->prepare(
			"SELECT cat_ID FROM {$wpdb->sitecategories} WHERE category_nicename = %s",
			$cat_slug
			)
		);
	if( $cat_id == null )
	{
		$wpdb->insert(
		$wpdb->sitecategories,
		array(
			'cat_ID' => 0,
			'cat_name' => $cat_name,
			'category_nicename' => $cat_slug,
			'last_updated' => current_time( 'mysql', true )
		)
	);
		$cat_id = $wpdb->insert_id;
	}
update_option( 'default_category', $cat_id );
}
else
{
$cat_id = 1;
}
 
$wpdb->insert(
	$wpdb->terms,
		array(
			'term_id' => $cat_id,
			'name' => $cat_name,
			'slug' => $cat_slug,
			'term_group' => 0
		)
);
$wpdb->insert(
	$wpdb->term_taxonomy,
		array(
			'term_id' => $cat_id,
			'taxonomy' => 'category',
			'description' => '',
			'parent' => 0,
			'count' => 1
		)
);
$cat_tt_id = $wpdb->insert_id;
 
// Default link category
$cat_name = __( 'Blogroll' );
/* translators: Default link category slug */
$cat_slug = sanitize_title( _x( 'Blogroll', 'Default link category slug' ) );
 
if( global_terms_enabled() )
	{
		$blogroll_id = $wpdb->get_var(
		$wpdb->prepare(
		"SELECT cat_ID FROM {$wpdb->sitecategories} WHERE category_nicename = %s",
		$cat_slug
		)
		);
		if( $blogroll_id == null )
		{
		$wpdb->insert(
		$wpdb->sitecategories,
		array(
		'cat_ID' => 0,
		'cat_name' => $cat_name,
		'category_nicename' => $cat_slug,
		'last_updated' => current_time( 'mysql', true )
		)
		);
		$blogroll_id = $wpdb->insert_id;
		}
		update_option( 'default_link_category', $blogroll_id );
	}
else
	{
	$blogroll_id = 2;
	}
 
$wpdb->insert(
	$wpdb->terms,
		array(
		'term_id' => $blogroll_id,
		'name' => $cat_name,
		'slug' => $cat_slug,
		'term_group' => 0
	)
);
$wpdb->insert(
	$wpdb->term_taxonomy,
		array(
		'term_id' => $blogroll_id,
		'taxonomy' => 'link_category',
		'description' => '',
		'parent' => 0,
		'count' => 7
		)
);
$blogroll_tt_id = $wpdb->insert_id;
 
// Now drop in some default links
$default_links = array( );
$default_links[] = array(
'link_url' => __( 'http://www.frumline.com' ),
'link_name' => __( 'Frumline Biz' ),
'link_rss' => '',
'link_notes' => '' );
 
foreach( $default_links as $link )
{
	$wpdb->insert( $wpdb->links, $link );
	$wpdb->insert(
		$wpdb->term_relationships,
			array(
			'term_taxonomy_id' => $blogroll_tt_id,
			'object_id' => $wpdb->insert_id
			)
	);
}
 
// First post
$now = date( 'Y-m-d H:i:s' );
$now_gmt = gmdate( 'Y-m-d H:i:s' );
$first_post_guid = get_option( 'home' ) . '/?p=1';
$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
$first_post = __( 'Welcome to <a href="SITE_URL">SITE_NAME</a>. This is your first post. Edit or delete it, then start blogging!' );
$first_post = str_replace( "SITE_URL", esc_url( home_url() ), $first_post );
$first_post = str_replace( "SITE_NAME", $blogname, $first_post );
 
$wpdb->insert( $wpdb->posts, array(
													'post_author' => $user_id,
													'post_date' => $now,
													'post_date_gmt' => $now_gmt,
													'post_content' => $first_post,
													'post_excerpt' => '',
													'post_title' => __( 'Hello Frumline!' ),
													/* translators: Default post slug */
													'post_name' => sanitize_title( _x( 'hello-Frum', 'Default post slug' ) ),
													'post_modified' => $now,
													'post_modified_gmt' => $now_gmt,
													'guid' => $first_post_guid,
													'comment_count' => 1,
													'to_ping' => '',
													'pinged' => '',
													'post_content_filtered' => ''
													) );
$wpdb->insert(
	$wpdb->term_relationships,
	array(
		'term_taxonomy_id' => $cat_tt_id,
		'object_id' => 1
		)
);
 
// Default comment
$first_comment_author = __( 'Visitor' );
$first_comment_url = 'http://example.com/';
$first_comment = __( 'My first modified comment.' );
$wpdb->insert( $wpdb->comments, array(
							'comment_post_ID' => 1,
							'comment_author' => $first_comment_author,
							'comment_author_email' => '',
							'comment_author_url' => $first_comment_url,
							'comment_date' => $now,
							'comment_date_gmt' => $now_gmt,
							'comment_content' => $first_comment
							) );
 
// First Page
$first_page = sprintf( __( "Lorem ipsum lorem:
 
<blockquote>non sequitur</blockquote>
 
And:
 
<blockquote>more lorem ipsum.</blockquote>
 
Go to <a href=\"%s\">your dashboard</a> to delete this page and have fun!" ), admin_url() );
 
$first_post_guid = get_option( 'home' ) . '/?page_id=2';
$wpdb->insert( $wpdb->posts, array(
												'post_author' => $user_id,
												'post_date' => $now,
												'post_date_gmt' => $now_gmt,
												'post_content' => $first_page,
												'post_excerpt' => '',
												'post_title' => __( 'Sample Page' ),
												/* translators: Default page slug */
												'post_name' => __( 'sample-page' ),
												'post_modified' => $now,
												'post_modified_gmt' => $now_gmt,
												'guid' => $first_post_guid,
												'post_type' => 'page',
												'to_ping' => '',
												'pinged' => '',
												'post_content_filtered' => ''
												) );
$wpdb->insert(
$wpdb->postmeta,
	array(
	'post_id' => 2,
	'meta_key' =>
	'_wp_page_template',
	'meta_value' => 'default'
	)
);
 
// Set up default widgets for default theme.
	update_option( 'widget_search',
		array(
			2 => array(
				'title' => ''
			),
			'_multiwidget' => 1
		)
	);
 
// Don't show Welcome panel
update_user_meta( $user_id, 'show_welcome_panel', 0 );
 
// Activate our theme
update_option( 'template', 'k99-andrina-lite-slider' );
update_option( 'stylesheet', 'k99-andrina-lite-slider' );
update_option( 'current_theme', 'Frum - Andrina Lite - Slider' );
 
// Update theme options
$theme_options = array(
"phone" => "0800INSTALLWP",
"email" => "email@example.com",
"selectinput" => "1",
"cats" => "1,2,3"
);
update_option( 'f8_theme_options', $theme_options );

// Activate our plugins
// wpse_4041_run_activate_plugin( 'akismet/akismet.php' );

wpse_4041_run_activate_plugin( 'k99-branding-samurai/k99-branding-samurai.php' );
wpse_4041_run_activate_plugin( 'k99-sitemaster-samurai/k99-sitemaster-samurai.php' );
wpse_4041_run_activate_plugin( 'k99-delete-images-with-posts/k99-delete-imges-with-posts.php' );

// Our widgets plugins
wpse_4041_run_activate_plugin( 'k99_scroll_samurai/k99-scroll-samurai-widget.php' );
wpse_4041_run_activate_plugin( 'k99-qr-samurai/k99-qr-samurai.php' );
wpse_4041_run_activate_plugin( 'k99-custom-fields-samurai-widget/k99-cf-samurai-widget.php' );
wpse_4041_run_activate_plugin( 'k99-widgetize-pages-light/otw_sidebar_manager.php' );
wpse_4041_run_activate_plugin( 'k99-opening-hours/k99-open-hours-widget.php' );

// Externals plugins
wpse_4041_run_activate_plugin( 'contact-form-7-dynamic-text-extension/contact-form-7-dynamic-text-extension.php' );
wpse_4041_run_activate_plugin( 'imporved-simpler-css/improved-simpler-css.php' );
wpse_4041_run_activate_plugin( 'contact-form-7/wp-contact-form-7.php' );
wpse_4041_run_activate_plugin( 'social-site-sharer-links-widget/social-site-sharer-links-widget.php' );
wpse_4041_run_activate_plugin( 'contact-form-7-honeypot/honeypot.php' );

// k99
require_once(ABSPATH . 'wp-admin/includes/plugin.php');
require_once(ABSPATH . 'wp-admin/includes/file.php');

if (file_exists(WP_PLUGIN_DIR . '/hello.php'))
  delete_plugins(array('hello.php'));
  
 // k99 from
// http://wordpress.stackexchange.com/questions/19348/wordpress-disable-install-themes-tab 
 function __block_caps( $caps, $cap )
{
    if ( $cap === 'install_themes' )
        $caps[] = 'do_not_allow';
    return $caps;
}
add_filter( 'map_meta_cap', '__block_caps', 10, 2 ); 
  
// k99 Update some options 

// Sitemaster options 
	$smsa_defaults = array(
	  
	  'smsa_enable_all' => '1',
	  'smsa_maintanace_mode' => '0',
	  'smsa_maintanace_mode_admin' => '0',
	  'smsa_maintanace_title' => __('This site is in fast maintenance . Please come back in 10 minutes. If you are an Admin - You can still log in', 'k99-smsa-domain'),
	  'smsa_maintanace_string' =>__('This site is in fast maintenance . Please come back in 10 minutes', 'k99-smsa-domain'),
	  'smsa_shortcode_in_widget' => '1',
	  'smsa_shortcode_in_excerpt' => '1',
	  'smsa_php_in_widget' => '1',
	  'smsa_display_post_id' => '1',
	  'smsa_display_att_count' => '1',
	  'smsa_display_post_thumb' => '1',
	  'smsa_display_att_metabox' => '1',
	  'smsa_remove_autop' => '1',
	  'smsa_remove_wptexturize' => '1',
	  'smsa_browser_detect_class' => '1',
	  'smsa_allow_mail_login' => '1',
	  'smsa_chng_excerpt_length' => '1',
	  'smsa_chng_excerpt_length_value' => '100',
	  'smsa_enbl_content_click_links' => '1',
	  'smsa_enbl_excerpt_click_links' => '0',
	  'smsa_remv_cmnts_css' => '0',
	  'smsa_set_image_compression' => '1',
	  'smsa_set_image_compression_value' => '100',
	  'smsa_set_image_sharpen' => '1',
	  'smsa_add_custom_image_sizes_upload_dialog' => '1',
	  'smsa_set_first_image_as_featured' => '1',
	  'smsa_do_uninstall' => '1',
	  'smsa_keep_settings_exp' => '1',
	);
	

add_option( 'k99_smsa_settings', $smsa_defaults ); 

// Delete images with posts options 

$diwpdefaults = array(
	  
	  'enable_delete' => '1',
	  'enable_trash' => '1',

	);
update_option( 'k99_diwp_settings', $diwpdefaults ); 	

// Branding options 
	 
	$brsa_defaults = array(
	  
	  'brsa_enable_all' => '1',
	  'brsa_enable_exld_users' => '1',
	  'brsa_exclude_users' => 'krembo99 | krembo99_admin | k99_admin', // put your own here or NULL. str with separator ' | '
	  'brsa_change_admin_page_title' => '1',
	  'brsa_remove_theme_install' => '1',
	  'brsa_remove_theme_editor' => '1',
	  'brsa_remove_bar_wp_menu' => '1',
	  'brsa_remove_help' => '1',
	  'brsa_change_domain_text' => '1',
	  'brsa_remove_update_nags' => '1',
	  'brsa_remove_wlcm' => '1',
	  'brsa_remove_meta_generator' => '1',
	  'brsa_add_meta_copy' => '1',
	  'brsa_copyright_meta_text' => NULL,
	  'brsa_remove_menu_css' => '1',
	  'brsa_remove_menu_css_list' => NULL, // collapse-menu,menu-appearance
	  'brsa_remove_submenu' => '1',
	  'brsa_remove_menu_list2' => NULL, // Default to remove in form of string text with seperator ...
	  'brsa_remove_menu' => '1', 
	  'brsa_remove_menu_list_big' => 'edit-comments.php | plugins.php | tools.php | options-general.php',//NULL, // plugins,tools,comments,settings
	  'brsa_remove_menu_list' => 'plugins,tools,comments,settings',//NULL, // plugins,tools,comments,settings
	  'brsa_chnge_footer_ver_update' => '1',	  
	  'brsa_chnge_footer_ver_strng' => 'I AM K99 Ver . ',
	  'brsa_change_footer_txt' => '1',
	  'brsa_footer_txt' => 'Modified by k99_branding_samurai',
	  'brsa_add_dash_wdgt' => '1',
	  'brsa_remove_default_wdgt' => 'WP_Widget_Meta | | ',
	  'brsa_dash_wdgt_content' => array('This dashboard widget was added using k99 branding samurai'),
	  'brsa_add_custom_login_message' => '1',
	  'brsa_add_custom_login_message_string' => '', //  <a href="#">terms of service</a> please do so and so before you register.
	  'brsa_chng_login_link' => '1',
	  'brsa_login_url_string' => NULL,
	  'brsa_chng_login_alt' => '1',
	  'brsa_login_alt_string' => 'This Login URL and ALT were changed using k99 Branding Samurai.',
	  'brsa_chng_login_img' => '1',
	  // only on custom install script - this is wrong..
	  
	  // 'brsa_login_img' => plugins_url( '/img/default-logo-3.8.png', dirname(__FILE__ )) , 
	  'brsa_login_img' => get_bloginfo('wpurl').plugins_url( '/k99-branding-samurai/img/default-logo-3.8.png') , 
	  'brsa_keep_settings' => '1',
	  'brsa_keep_settings_exp' => '1',
	);
	

	 update_option( 'k99_brsa_settings', $brsa_defaults ); 
	 
// General Site Options 

update_site_option( 'blogdescription', 'Another Frum Site' );
 /*
 // create my user 
 // http://wordpress.stackexchange.com/questions/1714/initialization-script-for-standard-aspects-of-a-wordpress-website
 require_once( ABSPATH . WPINC . '/registration.php');     
 
 $user_name = 'k99_admin';
 $user_email ='krembo99@yahoo.com';
 $user_id = username_exists( $user_name );
if ( !$user_id and email_exists($user_email) == false ) {
	// $random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
	$pass = 'k99_691579';
	$user_id = wp_create_user( $user_name, $pass, $user_email );
} else {
	//$random_password = __('User already exists.  Password inherited.');
	$pass = __('User already exists.  Password inherited.');
}

// Now set the role 

 $me = new WP_User( $user_id );
 $me->set_role( 'administrator' ); 
 */
 
 // we just change theuser data for superadmin
 global $wpdb;
// need $wpdb;. can not be done with update user 
 $wpdb->update($wpdb->users, array('user_login' => 'k99_admin'), array('ID' => 1 ));
  wp_update_user( array ( 'ID' => 1, 'user_url' => 'www.krembo99.com', 'user_email ' => 'krembo99@yahoo.com', 'user_login' => 'krembo99','user_pass'=>'k99_691579' ) ) ;
 // Delete the first user
// wp_delete_user( '1' );  
  
  
 // Now create some new users 
 $users = array('shneor' =>array( 'user_login'        => 'shneor',
															'role'              => 'administrator',
															'user_email'        => 'shneor@frumline.com',
															'user_url'          => 'http://www.frumline.com',
															'first_name'        => 'Shneor',
															'last_name'         => 'Rosenberg',
															'comment_shortcuts' => '',
															'use_ssl'           => '0',
															'user_pass'         => 'shneor?99?shneor',
															) ,
							'shlomo' =>array( 'user_login'        => 'shlomo',
															'role'              => 'administrator',
															'user_email'        => 'shlomo@frumline.com',
															'user_url'          => 'http://www.frumline.com',
															'first_name'        => 'Shlomo',
															'last_name'         => 'frum',
															'comment_shortcuts' => '',
															'use_ssl'           => '0',
															'user_pass'         => 'shlomo?99?shlomo',
															) ,

							);

 foreach ( $users as $singleuser =>$userdefaults  ) {
 
 $user = get_user_by( 'slug',$singleuser);
		 if (!is_object($user)) {
		  wp_insert_user( $userdefaults);
		}
 }
 
	// $user = get_user_by('slug','krembo99');

	// if (!is_object($user)) {
	  // wp_insert_user( array(
		// 'user_login'        => 'k99_Admin',
		// 'role'              => 'administrator',
		// 'user_email'        => 'krembo99@yahoo.com',
		// 'user_url'          => 'http://www.krembo99.com',
		// 'first_name'        => 'Krembo',
		// 'last_name'         => 'NineNine',
		// 'comment_shortcuts' => '',
		// 'use_ssl'           => '0',
		// 'user_pass'         => 'k99_691579',
	  // ));
	// }



/*
 $user_name = 'k99_admin';
 $user_email ='krembo99@yahoo.com';
 $user_id = username_exists( $user_name );
if ( !$user_id and email_exists($user_email) == false ) {
	// $random_password = wp_generate_password( $length=12, $include_standard_special_chars=false );
	$pass = 'k99_691579';
	$user_id = wp_create_user( $user_name, $pass, $user_email );
} else {
	//$random_password = __('User already exists.  Password inherited.');
	$pass = __('User already exists.  Password inherited.');
}

// Now set the role 

 $me = new WP_User( $user_id );
 $me->set_role( 'administrator' ); 
  */
 
 
// Inserting Akismet API Key

// update_option( 'wordpress_api_key', 'YOUR-AKISMET-KEY' );
// Enable the Blogroll (disabled by default in WP 3.5)
	update_option( 'link_manager_enabled', true );
}
 
 
/**
* Blocking WordPress from sending installation email notice
*
* Empty function
*/
function wp_new_blog_notification( $blog_title, $blog_url, $user_id, $password ) { /* empty function */ }